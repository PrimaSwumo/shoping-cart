import React from 'react';
import 'bootstrap/dist/css/bootstrap.css'
import './App.css'
import { Col, Container, ListGroup, Row, ThemeProvider } from 'react-bootstrap';
import 'bootstrap/dist/css/bootstrap.min.css';
// import 'bootstrap-icons/icons';
import gambar from './assets/kaoskuning.jpg'
import { useState } from 'react';




function App() {

const [jumlah,setJumlah]=useState(2)

    const handlePlus = ()=>{
      setJumlah(jumlah+1)
  }
  const handleMinus = ()=>{
    setJumlah(jumlah-1)
}

  let arrItemDiCart = [
    { gambar: <img className='ok w-100' src={gambar} alt={""}></img>, harga: 60, jumlah: jumlah, nama: "Blue denim shirt", ukuran: "M", warna: "merah" },
    { gambar: <img className='ok w-100' src={gambar} alt={""}></img>, harga: 41, jumlah: jumlah, nama: "Red hoodie", ukuran: "M", warna: "merah" },
    { gambar: <img className='ok w-100' src={gambar} alt={""}></img>, harga: 22, jumlah: jumlah, nama: "Black cap", ukuran: "M", warna: "merah" },
    { gambar: <img className='ok w-100' src={gambar} alt={""}></img>, harga: 83, jumlah: jumlah, nama: "Green run shoes", ukuran: "M", warna: "merah" },
    { gambar: <img className='ok w-100' src={gambar} alt={""}></img>, harga: 109, jumlah: jumlah, nama: "Brown glove", ukuran: "M", warna: "merah" },
    { gambar: <img className='ok w-100' src={gambar} alt={""}></img>, harga: 32, jumlah: jumlah, nama: "Golden ring", ukuran: "M", warna: "merah" }];


  return (
    <ThemeProvider breakpoints={['xl', 'lg', 'md', 'sm']} minBreakpoint='sm'>
      <Container>
        <Row>
          <Col md={12} className="oke2"><h4>Shopping cart</h4></Col>
        </Row>
        <Row>
          <Col md={6} lg={7} className='oke shadow p-3 mb-5 bg-body-tertiary rounded'>
            <Row>
              <Col className='oke1'><h5>Cart ({arrItemDiCart.length} items)</h5></Col>
            </Row>
            {arrItemDiCart.map(({ gambar, nama, warna, ukuran, jumlah, harga }, indexnya) =>
              <>
                <ItemCard gambar={gambar} harga={harga} jumlah={jumlah} nama={nama} ukuran={ukuran} warna={warna} />
                {indexnya == arrItemDiCart.length - 1 ? <></> : <Row>
                  <Col><hr></hr></Col>
                </Row>}
              </>)
            }
            {/* <ItemCard gambar={"gambar"} harga={60} jumlah={1} nama={"tshirt"} ukuran={"M"} />
            <Row>
              <Col><hr></hr></Col>
            </Row>
            <ItemCard gambar={"gambar"} harga={12} jumlah={1} nama={"denim"} ukuran={"M"} /> */}
            {/* <div className='namaCard'>
                <div className='nama'>Cart</div>
                <div className='jumlah'>(2 items)</div>
              </div>
          <div className='cardProduk'>
                <div className='gambar'></div>

                <div className='detail'>
                  <div className='detailKiri'>
                    <div className='namaBarang'>Blue denim shirt</div>
                    <div className='warna'>COLOR : Red</div>
                    <div className='ukuran'>SIZE : M</div>
                    <div className='pilihAksi'>
                      <div className='remove'>Remove item</div>
                      <div className='love'>Move to wish list</div>
                    </div>
                  </div>
                  <div className='detailKanan'>
                    <div className='quantity'>
                      <button className='minus'>-</button>
                      <div className='jumlah'>1</div>
                      <button className='plus'>+</button>
                    </div>
                    <div className='harga'>$17.99</div>
                  </div>
                </div>
              </div>
              <div className='cardProduk'>
                <div className='gambar'></div>
                <div className='detail'>
                  <div className='detailKiri'>
                    <div className='namaBarang'>Blue denim shirt</div>
                    <div className='warna'>COLOR : Red</div>
                    <div className='ukuran'>SIZE : M</div>
                    <div className='pilihAksi'>
                      <div className='remove'>Remove item</div>
                      <div className='love'>Move to wish list</div>
                    </div>
                  </div>
                  <div className='detailKanan'>
                    <div className='quantity'>
                      <button className='minus'>-</button>
                      <div className='jumlah'>1</div>
                      <button className='plus'>+</button>
                    </div>
                    <div className='harga'>$17.99</div>
                  </div>
                </div>
              </div> */}
          </Col>
          <Col md={5} lg={4}>
            <Row className='oke3 shadow p-3 mb-5 bg-body-tertiary rounded'>
              <Row>
                <Col><h5>The total amount of</h5></Col>
              </Row>
              <Row>
                <Col>
                  <Row>
                    <Col><p className="fs-6">Temporary amount</p></Col>
                    <Col><p className="fs-6 text-end">$53.98</p></Col>
                  </Row>
                  <Row>
                    <Col><p className="fs-6">Shipping</p></Col>
                    <Col><p className="fs-6 text-end">Gratis</p></Col>
                  </Row>
                  <Row>
                    <Col className='fw-bold'><p>The total amount of (including VAT)</p></Col>
                    <Col className='fw-bold text-end'><p>$53.98</p></Col>
                  </Row>
                  <Row>
                    <Col>
                      <div className="d-grid gap-2  ">
                        <button className="btn btn-primary btn-sm" type="button">GO TO CHECKOUT</button>
                      </div></Col>
                  </Row>
                </Col>
              </Row>
            </Row>
            <Row className='shadow p-3 mb-5 bg-body-tertiary rounded'>
              <Col>
              <select className="form-select form-select-sm" aria-label=".form-select-sm example">
  <option selected>Add a checkout code [optional]</option>
  <option value="1">Option one</option>
  <option value="2">Option two</option>
  <option value="3">Option three</option>
</select></Col>
            </Row>
          </Col>
          {/* <Col xs lg="2" className='oke'>
          1 of 3
        </Col> */}
          {/* <Col md="auto" className='oke'></Col> */}
          {/* <Col xs lg="2" className='oke'>
          3 of 3
        </Col> */}
          {/* </Row>
      <Row> */}
          {/* <Col className='oke2'>1 of 3</Col>
        <Col md="auto" className='oke2'>Variable width content</Col>
        <Col xs lg="2" className='oke2'>
          3 of 3
        </Col> */}
        </Row>
      </Container>
    </ThemeProvider>
  );
}

export default App;

export function ItemCard({ gambar, nama, warna, ukuran, jumlah, harga }) {
  return <Row>
    <Col sm={2}>{gambar}</Col>
    <Col xl={7} sm={10}>
      <Row>
        <Col className='fw-bold'><p className="fs-5">{nama}</p></Col>
      </Row>
      <Row>
        <Col><p className="fs-6">Color : {warna}</p></Col>
      </Row>
      <Row>
        <Col><p className="fs-6">Size : {ukuran}</p></Col>
      </Row>
      <Row sm={2}>
        <Col md={1}><svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-trash3" viewBox="0 0 16 16">
  <path d="M6.5 1h3a.5.5 0 0 1 .5.5v1H6v-1a.5.5 0 0 1 .5-.5ZM11 2.5v-1A1.5 1.5 0 0 0 9.5 0h-3A1.5 1.5 0 0 0 5 1.5v1H2.506a.58.58 0 0 0-.01 0H1.5a.5.5 0 0 0 0 1h.538l.853 10.66A2 2 0 0 0 4.885 16h6.23a2 2 0 0 0 1.994-1.84l.853-10.66h.538a.5.5 0 0 0 0-1h-.995a.59.59 0 0 0-.01 0H11Zm1.958 1-.846 10.58a1 1 0 0 1-.997.92h-6.23a1 1 0 0 1-.997-.92L3.042 3.5h9.916Zm-7.487 1a.5.5 0 0 1 .528.47l.5 8.5a.5.5 0 0 1-.998.06L5 5.03a.5.5 0 0 1 .47-.53Zm5.058 0a.5.5 0 0 1 .47.53l-.5 8.5a.5.5 0 1 1-.998-.06l.5-8.5a.5.5 0 0 1 .528-.47ZM8 4.5a.5.5 0 0 1 .5.5v8.5a.5.5 0 0 1-1 0V5a.5.5 0 0 1 .5-.5Z"/>
</svg></Col>
        <Col md={4} className='fw-semibold d-md-block d-none'><p><small>REMOVE ITEM</small></p></Col>
        <Col md={1}><svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-heart" viewBox="0 0 16 16">
  <path d="m8 2.748-.717-.737C5.6.281 2.514.878 1.4 3.053c-.523 1.023-.641 2.5.314 4.385.92 1.815 2.834 3.989 6.286 6.357 3.452-2.368 5.365-4.542 6.286-6.357.955-1.886.838-3.362.314-4.385C13.486.878 10.4.28 8.717 2.01L8 2.748zM8 15C-7.333 4.868 3.279-3.04 7.824 1.143c.06.055.119.112.176.171a3.12 3.12 0 0 1 .176-.17C12.72-3.042 23.333 4.867 8 15z"/>
</svg></Col>
        <Col className='fw-semibold d-md-block d-none'><p><small>MOVE TO WISH LIST</small></p></Col>
      </Row>
    </Col>
    <Col xl={2} md={12}>
      <Row className='oke4'>
        <Col sm={1}>
          <ListGroup horizontal>
            <ListGroup.Item action onClick={() => { }}>-</ListGroup.Item>
            <ListGroup.Item>{jumlah}</ListGroup.Item>
            <ListGroup.Item action onClick={handlePlus}>+</ListGroup.Item>
          </ListGroup>
        </Col>
      </Row>
      <Row >
        <Col></Col>
        <Col className='col-auto fw-bold text-end'>${harga}</Col>
      </Row>
    </Col>
  </Row>;
}
